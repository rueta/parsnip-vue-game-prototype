import type { Account } from '$lib/account';
import { accountServerCreateCustomToken, accountServerVerifySession } from '$lib/account.server';
import type { GetSession } from '@sveltejs/kit';
import { sequence } from '@sveltejs/kit/hooks';
import type { Handle } from '@sveltejs/kit/types/internal';
import Cookie from 'cookie';

// verify authorization via cookie
const hookVerifyAuth: Handle = async ({ event, resolve }) => {
	const res = resolve(event);

	const cookie = event.request.headers.get('cookie');
	if (!cookie) return res;

	const sess = Cookie.parse(cookie).session as string | undefined;
	const verify$ = await accountServerVerifySession(sess);

	// if got issue, tread as guest
	if (verify$.isError()) return res;

	const content = verify$.get();
	if (!content) return res;

	// pass decoded token id data to next nodes
	event.locals.decodedIdToken = content;
	return resolve(event);
};

// run in chain
export const handle: Handle = sequence(hookVerifyAuth);

// forward can public to front-size via [session] variable
export const getSession: GetSession = async ({ locals }) => {
	if (!locals.decodedIdToken) return {};

	// create new token
	const authToken$ = await accountServerCreateCustomToken(locals.decodedIdToken.uid);
	if (authToken$.isError()) return {};

	const d = locals.decodedIdToken;
	let auth: Account.AuthData | undefined = void 0;

	// we only forward data save to public
	// note: [clientToken] is the token client firebase auth library can use to signin
	if (d) {
		auth = {
			aud: d.aud,
			auth_time: d.auth_time,
			email: d.email,
			email_verified: d.email_verified,
			picture: d.picture,
			uid: d.uid,
			clientToken: authToken$.get()
		};
	}

	return {
		auth
	};
};
