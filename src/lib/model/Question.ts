// keep modal under namespace so in future we can add more types under Question
export namespace Question {
	/** Type is question type */
	export enum Type {
		Knowledge = 'knowledge_question',
		PictureAnswer = 'picture_answer',
		Multiselect = 'multiselect_questions',
		Matching = 'matching_questions',
		Ordering = 'ordering_question'
	}

	// because answer is nested data of [Question] document
	// we should keep them under same namespace so everybody can quickly
	// understand data structure.
	//#region
	export type KnowlegeAnswer = {
		title: string;
		letter: string;
		isCorrect: boolean;
		imageName: string;
		explainDescription: string;
		explainTitle: string;
		answerImage?: string;
	};

	export type PictureAnswer = {
		answerImage: string;
		explainDescription: string;
		explainTitle: string;
		filePath2: string;
		imageName: string;
		isCorrect: boolean;
		letter: string;
		media: Object;
		mediaItemId: string;
	};

	// more answer type ? define more here

	// let say we dont know all type of answers,
	// we will define a type and say it is common answer type.
	export type CommonAnswer = {
		title: string;
		letter: string;
		isCorrect: boolean;
		imageName: string;
		explainDescription: string;
		explainTitle: string;
		answerImage: string;
	};
	//#endregion

	// answer can have multi type follow question type.
	// ex: if question type is picture_answer then all answers
	// inside will be PictureAnswer type
	type Answers = KnowlegeAnswer[] | PictureAnswer[] | CommonAnswer[];

	/** Doc is question doc */
	export type Doc = {
		// define with enum, now use it inside component we will have
		// type hint.
		type: Type;
		id: string;
		order: number;
		question: string;
		randomize: boolean;
		levels: string[];
		createAt: number;
		image: string;
		isLive: boolean;
		levelId: string;
		media: string[];
		mediaId: string;
		explainDescription?: string;
		explainTitle?: string;
		answers: Answers;
	};
}
