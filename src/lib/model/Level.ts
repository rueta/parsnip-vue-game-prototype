export namespace Level {
  export type Doc = {
    category: string;
    createAt: number;
    id: string;
    isLive: boolean;
    order: number;
    questionIds: Array<string>;
    title: string;
  }
}