export namespace Dish {
  export type Doc = {
    createAt: number;
    id: string;
    image: string;
    isLive: boolean;
    levels: Array<string>;
    order: number;
    recipe: string;
    title: string;
    updateAt: number;
  }
}