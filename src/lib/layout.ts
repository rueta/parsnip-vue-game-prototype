// define stores / module related to layout tasks here.

import { writable } from 'svelte/store';

/** pageLoading handle page loading overlay */
export const pageLoading = writable(true);
