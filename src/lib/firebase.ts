import { browser } from '$app/env';
import { getApp, initializeApp, type FirebaseApp } from 'firebase/app';
import {
	// browserLocalPersistence,
	getAuth,
	initializeAuth,
	inMemoryPersistence,
	type Auth
} from 'firebase/auth';
import {
	enableIndexedDbPersistence,
	getFirestore,
	initializeFirestore,
	type Firestore
} from 'firebase/firestore';

/** kitFirebaseApp is firebase application library.
 *
 * Must init kit firebase before use.
 */
export let kitFirebaseApp: FirebaseApp;

/** kitFirebaseAuth is firebase authenticate library.
 *
 * Must init kit firebase before use.
 */
export let kitFirebaseAuth: Auth;

/** kitFirebaseFirestore is firebase firestore library.
 *
 * Must init kit firebase before use.
 */
export let kitFirebaseFirestore: Firestore;

// remember to avoid multi init
let inited = false;

/** kitFirebaseCreate init firebase libraries.
 *
 * This method must called before used in application.
 */
export const kitFirebaseInit = (config: any) => {
	if (inited) return;
	inited = true;

	try {
		kitFirebaseApp = getApp();

		kitFirebaseAuth = getAuth(kitFirebaseApp);

		kitFirebaseFirestore = getFirestore(kitFirebaseApp);
	} catch (error) {
		kitFirebaseApp = initializeApp(config);

		kitFirebaseAuth = initializeAuth(kitFirebaseApp, {
			// web use local storage to verify and save user information.
			// persistence: browserLocalPersistence
			persistence: inMemoryPersistence
		});

		kitFirebaseFirestore = initializeFirestore(kitFirebaseApp, {
			// tread undefined as non-save
			ignoreUndefinedProperties: true
		});

		if (browser) {
			enableIndexedDbPersistence(kitFirebaseFirestore)
				.then(() => console.log('firestore indexed db persistence working.'))
				.catch(() => console.warn('firestore indexed db persistence failed.'));
		}
	}
};
//#endregion
