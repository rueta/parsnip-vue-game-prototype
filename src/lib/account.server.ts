import { firebaseServerAuth } from '$lib/firebase.server';
import { Result } from '@swan-io/boxed';
import type { DecodedIdToken } from 'firebase-admin/auth';

export namespace AccountServer {}

/** accountServerVerifySession verify session string in cookie if available */
export const accountServerVerifySession = async (sess?: string) => {
	type R = Result<DecodedIdToken | undefined, unknown>;

	if (!sess) return Result.Ok(void 0) as R;

	const send = await Result.fromPromise(firebaseServerAuth.verifySessionCookie(sess));
	if (send.isError()) return Result.Error(send.getError()) as R;
	return Result.Ok(send.get()) as R;
};

/** accountServerCreateCustomToken read account uid and return new custom token
 * id one-time use.
 */
export const accountServerCreateCustomToken = async (uid: string) => {
	return Result.fromPromise(firebaseServerAuth.createCustomToken(uid));
};
