import { configSecret } from '$lib/config';
import { getApp, initializeApp } from 'firebase-admin/app';
import { getAuth } from 'firebase-admin/auth';
import { getFirestore } from 'firebase-admin/firestore';
import firebaseAdmin from 'firebase-admin';

// firebase-admin is commonjs
const { credential } = firebaseAdmin;

export namespace FirebaseServer {}

export let firebaseServerApp: ReturnType<typeof initializeApp>;
export let firebaseServerAuth: ReturnType<typeof getAuth>;
export let firebaseServerFirestore: ReturnType<typeof getFirestore>;

try {
	// try get again
	firebaseServerApp = getApp();
	firebaseServerAuth = getAuth(firebaseServerApp);
	firebaseServerFirestore = getFirestore(firebaseServerApp);
} catch (error) {
	// create new
	firebaseServerApp = initializeApp({
		credential: credential.cert(configSecret.firebaseKey)
	});
	firebaseServerAuth = getAuth(firebaseServerApp);
	firebaseServerFirestore = getFirestore(firebaseServerApp);

	// firestore config
	firebaseServerFirestore.settings({
		ignoreUndefinedProperties: true
	});
}
