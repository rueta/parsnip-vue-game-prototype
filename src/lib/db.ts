
// import { collection, getDocs } from '@firebase/firestore';
// import {kitFirebaseFirestore} from '$lib/firebase';
// import type { Question } from '$lib/model/Question';

// export async function getQuestions(): Promise<Question.Doc[]> {
// 	const colRef = collection(kitFirebaseFirestore, 'Question');
// 	const snapshot = await getDocs(colRef);

// 	return snapshot.docs.map((b) => {
// 		const data = b.data();
    
// 		return {
// 			id: b.id,
// 			type: data['type'],
//       order: data['order'],
//       question: data['question'],
//       randomize: data['randomize'],
//       levels: data['levels'],
//       answer: data['answer'],
//       createAt: data['createAt'],
//       image: data['image'],
//       isLive: data['isLive'],
//       levelId: data['levelId'],
//       media: data['media'],
//       mediaId: data['mediaId'],
// 		};
// 	});
// }