// we define static data, re-use datas here.
// It help code inside components more clear and avoid
// define again same data in components.

import { browser } from '$app/env';
import type { Question } from '$lib/model/Question';
import Sortable, { Swap } from 'sortablejs';

// alphabel array
//#region
export const alphabels = [
	'A',
	'B',
	'C',
	'D',
	'E',
	'F',
	'G',
	'H',
	'I',
	'J',
	'K',
	'L',
	'M',
	'N',
	'O',
	'P',
	'Q',
	'R',
	'S',
	'T',
	'U',
	'V',
	'W',
	'X',
	'Y',
	'Z'
];
//#endregion

/** appendAnswerLetter map alphabel as letter to answer list */
export const appendAnswerLetter = <T extends Question.Doc['answers']>(answers: T) => {
	return answers.map((i, inx) => {
		i.letter = alphabels[inx];
		return i as T[number];
	});
};

// use sortable
if (browser) {
	Sortable.mount(new Swap());
}
