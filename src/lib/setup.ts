// init hook before application start

import { accountInit } from '$lib/account';
import { configPublic } from '$lib/config';
import { kitFirebaseInit } from '$lib/firebase';

// init must-have & modules here
export const setupInit = () => {
	kitFirebaseInit(configPublic.firebaseKey);
	accountInit();
};
