import { browser } from '$app/env';
import { session } from '$app/stores';
import { Result } from '@swan-io/boxed';
import type { DecodedIdToken } from 'firebase-admin/auth';
import {
	onAuthStateChanged,
	signInWithCustomToken,
	signInWithEmailAndPassword,
	signOut,
	type User
} from 'firebase/auth';
import { get, writable } from 'svelte/store';
import './firebase';
import { kitFirebaseAuth } from './firebase';

export namespace Account {
	// Account data
	export type Data = undefined | null | User;

	/** AuthData is data parsed from session id from server
	 * and returned to UI via [session] variable.
	 */
	export type AuthData = Pick<
		DecodedIdToken,
		'aud' | 'auth_time' | 'email' | 'email_verified' | 'picture' | 'uid'
	> & {
		clientToken: string;
	};
}

// global
// url to endpoint create session
const createSessionApi = '/data/auth/create-session.json';
const removeSessionApi = '/data/auth/remove-session.json';

/** account is readable store self-update latest account information in web
 * use firebase auth lib.
 *
 * - User: user data
 * - null: guest (not login)
 * - undefined: module not working yet.
 */
export const account = writable<Account.Data>(void 0);

// init
//#region
export const accountInit = async () => {
	const { auth } = get(session);
	if (browser) {
		auth && (await signInWithCustomToken(kitFirebaseAuth, auth.clientToken));
		// auth firebase lib for web will take care read storage and verify.
		onAuthStateChanged(kitFirebaseAuth, (i) => account.set(i));
	}
};
//#endregion

// this file need init firebase first
// account module can working on svelte page some cases and main support
// browser only.

/** accountLoginByPassword login by email and password in page / client */
export const accountLoginByPassword = async (email: string, password: string) => {
	type R = Result<User, unknown>;

	// send request login use email and password via firebase auth web api
	const send = await Result.fromPromise(
		signInWithEmailAndPassword(kitFirebaseAuth, email, password)
	);

	if (send.isError()) return Result.Error(send.value) as R;

	// exchange current token id , get session id and new cookie.
	// this session later included every request and help server verify user.
	const tokenId = await send.value.user.getIdToken();
	const getSessId = await Result.fromPromise(
		fetch(createSessionApi, {
			method: 'POST',
			body: JSON.stringify({ tokenId })
		}).then((i) => i.json())
	);

	if (getSessId.isError()) return Result.Error(getSessId.value) as R;

	return Result.Ok(send.value.user) as R;
};

/** accountLogout logout current account */
export const accountLogout = async () => {
	type R = Result<true, unknown>;

	const signout$ = await Result.fromPromise(signOut(kitFirebaseAuth));
	if (signout$.isError()) return Result.Error(signout$.getError()) as R;

	// send request clear cookie session
	const clearSess$ = await Result.fromPromise(
		fetch(removeSessionApi, {
			method: 'POST'
		})
	);

	if (clearSess$.isError()) return Result.Error(clearSess$.getError()) as R;

	return Result.Ok(true) as R;
};
