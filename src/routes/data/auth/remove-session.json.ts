import type { RequestHandler } from '@sveltejs/kit';
import Cookie from 'cookie';

export const POST: RequestHandler = async () => {
	return {
		status: 200,
		headers: {
			'set-cookie': Cookie.serialize('session=1', '1', {
				httpOnly: true,
				sameSite: true,
				path: '/',
				secure: true,
				expires: new Date(1970)
			})
		},
		body: {}
	};
};
