import { firebaseServerAuth } from '$lib/firebase.server';
import type { RequestHandler } from '@sveltejs/kit';
import { Result } from '@swan-io/boxed';
import Cookie from 'cookie';

type Input = {
	tokenId: string;
};

export const POST: RequestHandler = async ({ request }) => {
	// get body
	const body = (await request.json()) as Input;
	const { tokenId } = body;

	if (!body.tokenId) {
		return {
			status: 401
		};
	}

	// Set session expiration to 5 days.
	const expiresIn = 60 * 60 * 24 * 5 * 1000;

	// Create the session cookie. This will also verify the ID token in the process.
	// The session cookie will have the same claims as the ID token.
	// To only allow session cookie setting on recent sign-in, auth_time in ID token
	// can be checked to ensure user was recently signed in before creating a session cookie.

	const createSess = await Result.fromPromise(
		firebaseServerAuth.createSessionCookie(tokenId, { expiresIn })
	);

	if (createSess.isError()) {
		return {
			status: 400,
			body: createSess.getError() as any
		};
	}

	const sess = createSess.get();

	return {
		status: 200,
		headers: {
			'set-cookie': Cookie.serialize('session', sess, {
				httpOnly: true,
				sameSite: true,
				path: '/',
				secure: true,
				maxAge: expiresIn
			})
		},
		body: {
			done: true
		}
	};
};
