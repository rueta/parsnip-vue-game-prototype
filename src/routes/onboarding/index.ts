import type { RequestHandler } from '@sveltejs/kit';
import type { Dish } from '$lib/model/Dish';
import { firebaseServerFirestore } from '$lib/firebase.server';

export const GET: RequestHandler = async ({ params }) => {
	
	
	let selectedDishes: Array<Dish.Doc> = [];

  //get all dishes
	const dishCollection = firebaseServerFirestore.collection('Dish');
	let dishes = await dishCollection.get();
	selectedDishes = dishes.docs.map(doc => doc.data()) as Array<Dish.Doc>

	

	return {
		status: 200,
		body: {
			dishes: selectedDishes
		}
	};
};