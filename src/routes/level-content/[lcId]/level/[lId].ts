import type { RequestHandler } from '@sveltejs/kit';
import type { Level } from '$lib/model/Level';
import type { Question } from '$lib/model/Question';
import { firebaseServerFirestore } from '$lib/firebase.server';

export const GET: RequestHandler = async ({ params }) => {
	type Param = {
		lId: string;
		lcId: string
	};

	const { lId, lcId } = params as Param;

	let selectedlevel: Level.Doc;
	let selectedQuestion: Question.Doc;
	let selectedQuestions: Array<Question.Doc & Record<string, any>> = [];

	const levelContentCollection = firebaseServerFirestore.collection('LevelContent');
	let level = await levelContentCollection.doc(lId).get();
	selectedlevel = level.data() as Level.Doc;

	const questionCollection = firebaseServerFirestore.collection('Question');

	const getQuestions = selectedlevel.questionIds.map(async (id) => {
		const question = await questionCollection.doc(id).get();
		selectedQuestion = question.data() as Question.Doc;
		selectedQuestions.push(selectedQuestion);
	});

	await Promise.all(getQuestions);

	return {
		status: 200,
		body: {
			selectedlevel: selectedlevel,
			questions: selectedQuestions,
			lcId: lcId,
			lId: lId,
		}
	};
};
