import type { RequestHandler } from '@sveltejs/kit';
import type { Dish } from '$lib/model/Dish';
import type { Level } from '$lib/model/Level';
import { firebaseServerFirestore } from '$lib/firebase.server';

export const GET: RequestHandler = async ({ params }) => {
	// const { decodedIdToken } = locals;

	// if (!decodedIdToken) {
	// 	return {
	// 		status: 401
	// 	};
	// }

	type Param = {
		lcId: string;
	};

	const { lcId } = params as Param;
	// you have id from url !
	let selectedDish: Dish.Doc;
	let selectedlevel: Level.Doc;
	let selectedlevels: Array<Level.Doc> = [];
	let selectedDishes: Array<Dish.Doc> = [];
	let randomDish: Dish.Doc

	//get all dishes 
	const dishCollection = firebaseServerFirestore.collection('Dish');
	let dishes = await dishCollection.get();
	selectedDishes = dishes.docs.map(doc => doc.data()) as Array<Dish.Doc>
	selectedDishes = selectedDishes.filter(dish => dish.isLive)
	randomDish = selectedDishes[Math.floor(Math.random()*selectedDishes.length)];

  //get dish by id
	let dish = await dishCollection.doc(lcId).get();
	selectedDish = dish.data() as Dish.Doc;
	
	// get level documents under dish
	const levelCollection = firebaseServerFirestore.collection('LevelContent');
	const getLevels = selectedDish.levels.map(async (id) => {
		const level = await levelCollection.doc(id).get();

		selectedlevel = level.data() as Level.Doc;
		selectedlevel.id = id

		// no need to remap array and re-assigned everytime , we only
		// use this way with svelte since svelte not react with array's method
		// in nodejs it is static so we just need push item
		// selectedlevels = [...selectedlevels, selectedlevel];
		selectedlevels.push(selectedlevel);
	});

	await Promise.all(getLevels);
	
	return {
		status: 200,
		body: {
			dish: selectedDish,
			levels: selectedlevels,
			lcId: lcId,
			randomDishId: randomDish.id
		}
	};
};
