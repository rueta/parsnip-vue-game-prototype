/// <reference types="@sveltejs/kit" />

// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
// and what to do when importing types
declare namespace App {
	type DecodedIdToken = import('firebase-admin/auth').DecodedIdToken;
	type AuthData = import('$lib/account').Account.AuthData;

	interface Locals {
		/** auth is decoded session content via hook */
		decodedIdToken?: DecodedIdToken;
	}
	// interface Platform {}
	interface Session {
		auth?: AuthData;
	}
	// interface Stuff {}
}
