const plugin = require('tailwindcss/plugin');
const defaultTheme = require('tailwindcss/defaultTheme');
const themeLight = require('daisyui/src/colors/themes')['[data-theme=light]'];

// color definition
const colorGreenNeutral = '#51a045';

/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		// keyframes
		//#region
		keyframes: {
			startPageLogo: {
				'0%': {
					height: '70px',
					opacity: '0'
				},
				'50%': {
					height: '200px',
					transform: 'translateX(0)',
					opacity: '1'
				},
				'100%': {
					height: '70px',
					transform: 'translateX(-80px)',
					opacity: '1'
				}
			},
			startPageName: {
				'0%': {
					transform: 'translateX(-40px)',
					opacity: 0
				},
				'50%': {
					opacity: 0.5
				},
				'100%': {
					opacity: 1
				}
			},
			wrongAnswer: {
				'0%': {
					transform: 'translateX(10px)',
					border: '2px solid #E95230'
				},
				'20%': {
					transform: 'translateX(-10px)',
					border: '2px solid #E95230'
				},
				'40%': {
					transform: 'translateX(10px)',
					border: '2px solid #E95230'
				},
				'60%': {
					transform: 'translateX(-10px)',
					border: '2px solid #E95230'
				},
				'80%': {
					transform: 'translateX(10px)',
					border: '2px solid #E95230'
				},
				'100%': {
					transform: 'translateX(0)',
					border: '2px solid #E95230'
				}
			},
			wrongMatchingImage: {
				'0%': {
					transform: 'translateX(3px)',
					border: '1px solid #E95230'
				},
				'70%': {
					border: '1px solid #E95230'
				},
				'100%': {
					transform: 'translateX(-3px)'
				}
			},
			startPage: {
				from: {
					opacity: 0
				},
				to: {
					opacity: 1
				}
			},
			zoom: {
				from: {
					transform: 'scale(0.7)'
				},
				to: {
					transform: 'scale(1)'
				}
			},
		},
		//#endregion

		// animation
		//#region
		animation: {
			startPageLogo: 'startPageLogo 1.5s ease-in-out forwards',
			startPageName: 'startPageName 0.3s ease forwards',
			wrongAnswer: 'wrongAnswer 0.3s',
			wrongMatchingImage: 'wrongMatchingImage 0.2s',
			startPage: 'startPage 1s',
			zoom: 'zoom 0.5s'
		},
		//#endregion

		// font family
		//#region
		fontFamily: {
			sans: [`'Avenir LT Std'`, ...defaultTheme.fontFamily.sans],
			serif: [`'Filson Soft'`, ...defaultTheme.fontFamily.serif],
			avenir: ['Avenir LT Std'],
			filson: ['Filson Soft'],
		},
		//#endregion

		// custom for plugin
		delayTime: {
			100: '100ms',
			200: '200ms',
			500: '500ms'
		},

		extend: {
			colors: {
				grey: {
					dark: '#222222',
					light: '#EDEBE8',
					neutral: '#919090',
					'neutral-light': '#BDBBB7'
				},
				green: {
					neutral: colorGreenNeutral
				},
				yellow: {
					light: '#EDA81C'
				}
			},
			width: {
				343: '343px'
			},
			height: {
				193: '193px'
			},
			padding: {
				'4px': '4px 0px 4px 1px'
			},
			borderRadius: {
				large: '48px'
			},
		}
	},
	plugins: [
		require('daisyui'),
		// add more missing style
		plugin(({ matchUtilities, theme }) => {
			// match class
			// can use with predicted values ex: animation-delay-100
			// can use with arbitrary ex: animation-delay-[2s]
			matchUtilities(
				{
					'animation-delay': (value) => ({
						'animation-delay': value
					})
				},
				{
					values: theme('delayTime')
				}
			);
		})
	],

	daisyui: {
		styled: true,
		themes: [
			{
				light: {
					...themeLight,
					primary: colorGreenNeutral
				}
			}
		],
		base: true,
		utils: true,
		logs: false,
		rtl: false,
		prefix: '',
		darkTheme: 'dark'
	}
};
